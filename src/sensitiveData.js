/**
 * This is the Google Maps API Key used by the webapp.
 * Under normal circumstances, this file should not be committed to the
 * repo and it should be git-ignored.
 * However, since the API key has been restricted, I will include it
 * for the purpose of this exercise.
 */
export const gMapApiKey = 'AIzaSyCVt200DBoeoAp6k-gpmyLfILx4w4aS2zg'
