import setupGoogleMock from './setupGoogleMock'
import { findAddresses } from './utils'

let setGeocodeResults = null

beforeAll(() => {
  setGeocodeResults = setupGoogleMock()
})

it('should reject because of missing coordinates', async (done) => {
  try {
    await findAddresses([])
  }
  catch (e) {
    expect(e).toBe('missing coordinates')
    done()
  }
})

it('should reject because of bad status', async (done) => {
  setGeocodeResults({ status: 'KO', results: null })
  try {
    await findAddresses([43.48292, 11.39284])
  }
  catch (e) {
    expect(e).toMatch('cannot reverse geocode')
    done()
  }
})

it('should collapse equal addresses', async (done) => {
  const testAddresses = [ 'ADDR1', 'ADDR2', 'ADDR1' ]
  const results = testAddresses.map(a => ({ formatted_address: a }))
  setGeocodeResults({ status: 'OK', results })
  const addresses = await findAddresses([43.48292, 11.39284])
  expect(addresses).toHaveLength(2)
  expect(addresses).toEqual(expect.arrayContaining(['ADDR1', 'ADDR2']))
  done()
})
