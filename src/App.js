import React, { Component } from 'react'
import { Map, InfoPanel } from './components'
import { findAddresses, sleep } from './utils'
import './App.css'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      position: null,  // clicked position (eg. [43.1112, 22.454])
      addresses: [],  // addresses found after reverse geocoding
      searching: false,  // tells whether reverse geocoding is in progress
      error: false,  // tells whether errors occur during reverse geocoding
    }
    this.onMapClick = this.onMapClick.bind(this)
  }

  componentDidMount() {
    document.title = 'VZC Map'
  }

  async onMapClick(position) {
    await this.setState({ position, searching: true, error: false })

    try {
      const addresses = await findAddresses(position)

      // this sleep call is used for UX purposes only, so that it is clear
      // that a loading indicator appears during reverse geocoding process
      await sleep(500)

      this.setState({ searching: false, addresses })
    }
    catch (e) {
      this.setState({ searching: false, addresses: [], error: true })
    }
  }

  render() {
    const { position, addresses, searching, error } = this.state

    return (
      <div className="App">
        <Map
          customClassName="App__MapPanel"
          markerPosition={position}
          clickDisabled={searching}
          onClick={this.onMapClick}
        />
        <div className="App__rightColumn">
          <InfoPanel
            customClassName="App__InfoPanel"
            searching={searching}
            position={position}
            addresses={addresses}
            error={error}
          />
        </div>
      </div>
    )
  }
}

export default App
