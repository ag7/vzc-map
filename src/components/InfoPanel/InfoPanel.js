import React, { Component } from 'react'
import PropTypes from 'prop-types'
import posed from 'react-pose'
import Carousel from '../Carousel'
import { BounceLoader as Loader } from 'react-spinners'
import './InfoPanel.css'


// build needed components for react-pose animations
const IconBox = posed.div({
  centered: { left: '115px' },
  leftside: { left: '30px' },
})

const FadeBox = posed.div({
  visible: { opacity: 1, transition: { delay: 300 } },
  hidden: { opacity: 0 },
})


export default class InfoPanel extends Component {

  renderPosition(position) {
    return (
      <div>
        <span className="InfoPanel__Label"><b>LATITUDE</b></span><br/>
        <span>{position[0].toFixed(6)}</span>
        <div className="InfoPanel__LatLngSpacer"></div>
        <span className="InfoPanel__Label"><b>LONGITUDE</b></span><br/>
        <span>{position[1].toFixed(6)}</span>
      </div>
    )
  }

  renderPlaceholder() {
    return (
      <div className="InfoPanel__secondRowDiv">
        <span>CLICK MAP TO FIND ADDRESS</span>
      </div>
    )
  }

  renderIconRow() {
    const { position } = this.props
    return (
      <div className="InfoPanel__IconRow">
        <IconBox className="InfoPanel__IconDiv" pose={position ? 'leftside' : 'centered'}>
          <img
            className="InfoPanel__Icon"
            alt="marker icon"
            src={require('../assets/info-pointer.png')}
          />
        </IconBox>
        <FadeBox className="InfoPanel__LatLng" pose={position ? 'visible' : 'hidden'}>
          {position && this.renderPosition(position)}
        </FadeBox>
      </div>
    )
  }

  renderLoader() {
    return (
      <div className="InfoPanel__loading">
        <Loader
          sizeUnit="px"
          color={'rgb(166,229,212)'}
          size={40}
          loading={this.props.searching}
        />
      </div>
    )
  }

  renderAddresses() {
    const { addresses, error } = this.props

    if (error || !addresses || addresses.length <= 0) {
      return (
        <div className="InfoPanel__notFound">
          {error ? 'GEOCODING ERROR' : 'NO ADDRESS FOUND'}
        </div>
      )
    }

    return (
      <Carousel
        items={addresses}
        renderFooter={() => {
          return (
            <div className="InfoPanel__searchAgain">CLICK ON MAP TO SEARCH AGAIN</div>
          )
        }}
      />
    )
  }

  renderResultsRow() {
    return (
      <FadeBox
        className="InfoPanel__secondRowDiv InfoPanel__loadingDiv"
        initialPose="hidden" pose="visible"
      >
        {this.props.searching ? this.renderLoader() : this.renderAddresses()}
      </FadeBox>
    )
  }

  render() {
    const { customClassName, position } = this.props
    return (
      <div className={customClassName}>
        {this.renderIconRow()}
        {position ? this.renderResultsRow() : this.renderPlaceholder()}
      </div>
    )
  }
}

InfoPanel.propTypes = {
  customClassName: PropTypes.string,
  position: PropTypes.arrayOf(PropTypes.number),
  addresses: PropTypes.arrayOf(PropTypes.string).isRequired,
  searching: PropTypes.bool.isRequired,
  error: PropTypes.bool.isRequired,
}
