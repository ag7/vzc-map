import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Carousel } from 'react-bootstrap/lib'
import './Carousel.css'

export default class MyCarousel extends Component {
  constructor(props) {
    super(props)
    this.state = { currentIndex: 0 }
    this.onPrevClick = this.onPrevClick.bind(this)
    this.onNextClick = this.onNextClick.bind(this)
  }

  UNSAFE__componentWillReceiveProps() {
    this.setState({ currentIndex: 0 })
  }

  onPrevClick(e) {
    e.preventDefault()
    const { currentIndex } = this.state
    if (currentIndex > 0) {
      this.setState({ currentIndex: currentIndex - 1 })
    }
  }

  onNextClick(e) {
    e.preventDefault()
    const { currentIndex } = this.state
    const { items } = this.props
    if (currentIndex < items.length - 1) {
      this.setState({ currentIndex: currentIndex + 1 })
    }
  }

  render() {
    const { items, renderFooter } = this.props
    const { currentIndex } = this.state

    const prevButtonDisabled = (currentIndex <= 0) ? 'disabled' : ''
    const nextButtonDisabled = (currentIndex >= items.length - 1) ? 'disabled' : ''

    return (
      <div className="Carousel__container">
        <Carousel
          className="Carousel__bootstrapElement"
          activeIndex={currentIndex}
          onSelect={() => {}}
          controls={false}
          indicators={false}
        >
          {
            items.map((item, idx) => {
              return (
                <Carousel.Item key={idx} className="Carousel__item">
                  <div className="Carousel__itemDiv">
                    {item}
                  </div>
                </Carousel.Item>
              )
            })
          }
        </Carousel>
        {items.length > 1 &&
          <div className="Carousel__controls">
            <button
              className={`btn btn-link btn-sm Carousel__control ${prevButtonDisabled}`}
              onClick={this.onPrevClick}
            >
              PREV
            </button>
            <div className="Carousel__controlSpacer"></div>
            <button
              className={`btn btn-link btn-sm Carousel__control ${nextButtonDisabled}`}
              onClick={this.onNextClick}
            >
              NEXT
            </button>
          </div>
        }
        {renderFooter && renderFooter()}
      </div>
    )
  }
}

MyCarousel.propTypes = {
  items: PropTypes.arrayOf(PropTypes.string).isRequired,
  renderFooter: PropTypes.func,
}
