import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps'
import { gMapApiKey } from '../../sensitiveData'
import './Map.css'


// build actual map component
const GMap = withScriptjs(withGoogleMap(GoogleMap))

// some gmap-related constants
const gMapBaseURL = 'https://maps.googleapis.com/maps/api/js'
const gMapVer = '3.exp'
const gMapLibraries = 'geometry,drawing,places'
const gMapURL = `${gMapBaseURL}?key=${gMapApiKey}&v=${gMapVer}&libraries=${gMapLibraries}`

const mapOptions = {
  fullscreenControl: false,  // disable fullscreen button
  streetViewControl: false,  // disable street view control
  clickableIcons: false,
}

export default class Map extends Component {
  constructor(props) {
    super(props)
    this.onMapClick = this.onMapClick.bind(this)
  }

  onMapClick(gmapEvent) {
    const { clickDisabled, onClick } = this.props
    if (!clickDisabled) {
      onClick([gmapEvent.latLng.lat(), gmapEvent.latLng.lng()])
    }
  }

  renderLoadingElement() {
    return (
      <div className="Map__element Map__loading">
        Map is currently loading...
      </div>
    )
  }

  render() {
    const { customClassName, markerPosition } = this.props

    return (
      <GMap
        googleMapURL={gMapURL}
        loadingElement={this.renderLoadingElement()}
        containerElement={<div className={customClassName} />}
        mapElement={<div className="Map__element" />}
        defaultZoom={16}
        defaultCenter={{ lat: 43.781840, lng: 11.233947 }}
        defaultOptions={mapOptions}
        onClick={this.onMapClick}
      >
        {!!markerPosition &&
          <Marker position={{ lat: markerPosition[0], lng: markerPosition[1] }} />
        }
      </GMap>
    )
  }
}


Map.propTypes = {
  customClassName: PropTypes.string,
  markerPosition: PropTypes.arrayOf(PropTypes.number),
  clickDisabled: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
}
