
// Google Maps geocoder (singleton)
let geocoder = null

export const findAddresses = ([latitude, longitude]) => new Promise((resolve, reject) => {
  // geocoder lazy instantiation
  if (!geocoder) {
    geocoder = new window.google.maps.Geocoder()
  }

  // no geocoder => give up
  if (!geocoder) {
    reject('cannot instantiate geocoder')
    return
  }

  // no coordinates => give up
  if (!latitude || !longitude) {
    reject('missing coordinates')
    return
  }

  // perform reverse geocoding
  const location = { lat: parseFloat(latitude), lng: parseFloat(longitude) }
  geocoder.geocode({ location }, (results, status) => {
    // some errors occurred
    if (status.toLowerCase() !== 'ok') {
      reject(`cannot reverse geocode ${JSON.stringify(location)}`)
      return
    }

    // sometimes Google returns multiple results with same formatted address
    // so we collapse them
    const unifiedAddresses = new Set()
    results.map(r => unifiedAddresses.add(r.formatted_address))

    // return unique formatted addresses
    resolve(Array.from(unifiedAddresses))
  })
})

export const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms))
