# VZC Map

Interactive map with reverse geocoding capabilities.  
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Quick Start 
To run the webapp you need to have `node` installed in the system (https://nodejs.org/en/).  
You can use for example the latest LTS (10.13.0).  

After that, clone the project and:

* install dependency with `npm install` or `yarn install`
* run the webapp with `npm start` or `yarn start`

Webapp will be available on `localhost:3000`.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.
The build is minified and the filenames include the hashes.<br>

### `npm run lint`

Execute `eslint` for javascript files inside the `src` folder and output results.
